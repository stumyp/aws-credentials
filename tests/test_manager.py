from unittest.mock import PropertyMock, patch

from aws_credentials.manager import Manager


def test_manager_uses_first_active_key_when_not_set(iam, stub_list):
    iam_stubber, iam_client = iam
    list_response = stub_list(iam_stubber)
    key = list_response['AccessKeyMetadata'][0]['AccessKeyId']

    with patch.object(Manager, 'client', new_callable=PropertyMock, return_value=iam_client):
        mgr = Manager()
    assert key == mgr.aws_access_key_id
